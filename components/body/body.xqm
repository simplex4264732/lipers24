module namespace content = "content";

declare function content:main($params as map(*)){
  let $body :=
    $params?page ?? $params?_("pages/" || $params?page, $params) !! <a href="/u/teachers.list">Расписание учителей</a>
  return
    map{
      "контент": $body
    }
};