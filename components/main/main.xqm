module namespace main = "main";

declare function main:main($params as map(*)){
  map{
    'header' : $params?_("header", map{}),
    'body' : $params?_("body", $params),
    'footer' : $params?_("footer", map{})
  }
};