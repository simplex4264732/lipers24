module namespace teachers.list = "teachers.list";

declare function teachers.list:main($params as map(*)){
  let $rdf := 
    fetch:xml(
      $params?_config("rdfHost") || "/trci-to-rdf/api/v01/components/%D1%81%D0%BF%D0%B8%D1%81%D0%BE%D0%BA-%D1%83%D1%87%D0%B8%D1%82%D0%B5%D0%BB%D0%B5%D0%B9"
    )
  let $список := 
    for $i in $rdf//li/text()
    let $href := 
      web:create-url(
        "/u/teacher.shedule",
        map{
          "учитель":$i
        }
      )
    return
      <li><a href="{$href}">{$i}</a></li>
  return
    map{
      "контент":$список
    }
};