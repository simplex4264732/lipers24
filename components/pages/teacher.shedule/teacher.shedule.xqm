module namespace teacher.shedule= "teacher.shedule";

declare function teacher.shedule:main($params as map(*)){
  let $rdf := 
    fetch:xml(
      web:create-url(
        $params?_config("rdfHost") || "/trci-to-rdf/api/v01/components/расписание-учителя",
        map{
          "учитель": request:parameter("учитель")
        }
      )
    )
  return
    map{
      "контент":$rdf
    }
};