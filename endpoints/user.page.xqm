module namespace lipers-rdf = "lipers24.ru/rdf/v0.1/user";

import module namespace funct="simplex-frame/v0.1/funct" at "../../simplex-frame/functions.xqm";

declare 
  %rest:GET
  %rest:path("/u/{$page}")
  %output:method("xhtml")
  %output:doctype-public("www.w3.org/TR/xhtml11/DTD/xhtml11.dtd")
function lipers-rdf:main($page){
  funct:tpl('main', map{'page':$page})
};